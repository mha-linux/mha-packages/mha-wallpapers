#!/bin/bash
while true
do
	ls /usr/share/mha-wallpapers/images |sort -R |tail -$N |while read file; do
		cp /usr/share/mha-wallpapers/images/$file ~/.background.jpg
	done
	nitrogen --set-zoom-fill ~/.background.jpg
	betterlockscreen -u ~/.background.jpg
	sleep 300
done
